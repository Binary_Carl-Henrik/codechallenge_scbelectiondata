﻿using System;
using System.Collections.Generic;

namespace Start
{
    class JSONSendCommand
    {
        public Values[] query { get; set; }
        public Response response { get; set; }
    }

    public class Response
    {
        public string format { get; set; }
    }

    public class Values
    {
        public string code { get; set; }
        public Selection selection { get; set; }
    }

    public class Selection
    {
        public string filter { get; set; }
        public List<string> values { get; set; }
    }
}
