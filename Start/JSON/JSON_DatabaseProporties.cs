﻿using System.Collections.Generic;

namespace Start
{
    class JSON_DatabaseProporties
    {
        public string title { get; set; }
        public Variables[] variables { get; set; }
    }
    public class Variables
    {
        public string code { get; set; }
        public string text { get; set; }
        public List<string> values { get; set; }
        public List<string> valueTexts { get; set; }
    }
}
