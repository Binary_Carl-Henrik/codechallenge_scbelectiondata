﻿using System.Collections.Generic;

namespace Start
{
    public class SCBData_Election
    {
        public List<Column> columns { get; set; }
        public List<Comment> comments { get; set; }
        public List<Data> data { get; set; }
    }
    public class Column
    {
        public string code { get; set; }
        public string text { get; set; }
        public string type { get; set; }
    }

    public class Comment
    {
        public string variable { get; set; }
        public string value { get; set; }
        public string comment { get; set; }
    }

    public class Data
    {
        public List<string> key { get; set; }
        public List<string> values { get; set; }
    }

    
}
