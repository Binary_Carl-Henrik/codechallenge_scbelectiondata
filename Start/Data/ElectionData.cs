﻿using System.Collections.Generic;
using System.Linq;

namespace Start
{
    class ElectionData
    {
        public double Percent { private set; get; }
        public List<string> City { private set; get; }

        public string Year { private set; get; }
        public ElectionData(string year)
        {
            City = new List<string>();
            Percent = 0;
            Year = year;
        }

        public void updateCity(string city, string percent)
        {
            double replacer = 0;
            if (percent.Equals("--"))
            {
                
            }
            percent = percent.Replace('.', ',');
            
            double.TryParse(percent, out replacer);
            if (replacer > Percent)
            {
                Percent = replacer;
                City.Clear();
                City.Add(city);
            }else if(replacer == Percent)
            {
                City.Add(city);
            }
        }

        public string getString(JSON_DatabaseProporties proporties)
        {
            string retString = Year + " ";
            for(int i = 0; i<City.Count; i++)
            {
                retString += getCityName(City.ElementAt(i), proporties);
                if(City.Count != i + 1)
                {
                    retString += ", ";
                }
            }
            retString += " " + Percent + "%";
            return retString;
        }

        private string getCityName(string city, JSON_DatabaseProporties proporties)
        {
            for(int i = 0; i<proporties.variables[0].values.Count; i++)
            {
                if (city.Equals(proporties.variables[0].values.ElementAt(i)))
                {
                    return proporties.variables[0].valueTexts.ElementAt(i);
                }
            }
            return city;
        }
    }
}
