﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;

namespace Start
{   
    class Program
    {
        static HttpClient client = new HttpClient();
        
        static void Main()
        {
            RunAsync().Wait();
        }
        static async Task RunAsync()
        {
            try
            {
                JSON_DatabaseProporties proporties = await GetProportiesAsync("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/ME/ME0104/ME0104D/ME0104T4");

                string rawJsonData = await getDataFromJsonCommandAsync(proporties);
                SCBData_Election dataOfElections = JsonConvert.DeserializeObject<SCBData_Election>(rawJsonData);

                showYears(findLargestElectionParticipationPerYear(dataOfElections), proporties);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }

        static async Task<JSON_DatabaseProporties> GetProportiesAsync(string path)
        {
            JSON_DatabaseProporties proporties = null;
            HttpResponseMessage response = await client.GetAsync(path);

            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                proporties = await response.Content.ReadAsAsync<JSON_DatabaseProporties>();
            }
            return proporties;
        }
        
        static async Task<string> getDataFromJsonCommandAsync(JSON_DatabaseProporties proporties)
        {
            proporties = removeUnwantedValues(proporties);

            Values[] query = new Values[] { getSendValues(proporties.variables[0].code, proporties.variables[0].values), getSendValues(proporties.variables[1].code, getNewListFromSingleValue(proporties.variables[1].values.ElementAt(0))) };
            
            string commandString = JsonConvert.SerializeObject(getSendCommand(query, "json"));
            
            return await getDataAsync(commandString);
        }

        static JSON_DatabaseProporties removeUnwantedValues(JSON_DatabaseProporties proporties)
        {
            int index = proporties.variables[0].values.IndexOf("00");
            proporties.variables[0].values.RemoveAt(index);
            proporties.variables[0].valueTexts.RemoveAt(index);
            return proporties;
        }

        static List<string> getNewListFromSingleValue(string value)
        {
            List<string> list = new List<string>();
            list.Add(value);
            return list;
        }

        static Values getSendValues(string code, List<string> Value)
        {
            Selection selec = new Selection();
            selec.filter = "";
            selec.values = Value;

            Values value = new Values();
            value.code = code;
            value.selection = selec;

            return value;
        }

        static JSONSendCommand getSendCommand(Values[] query, string responseFormat)
        {
            JSONSendCommand sender = new JSONSendCommand();
            sender.query = query;
            Response response = new Response();
            response.format = responseFormat;
            sender.response = response;

            return sender;
        }

        static async Task<string> getDataAsync(string command)
        {
            string dataString = "";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://api.scb.se/OV0104/v1/doris/sv/ssd/START/ME/ME0104/ME0104D/ME0104T4");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(command);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                dataString = await streamReader.ReadToEndAsync();
            }
            return dataString;
        }

        static List<ElectionData> findLargestElectionParticipationPerYear(SCBData_Election electionData)
        {
            List<ElectionData> years = new List<ElectionData>();
            foreach (Data date in electionData.data)
            {
                int indexOfYears = ContainsYear(date.key.ElementAt(1), years);
                if (indexOfYears < 0)
                {
                    ElectionData singleYear = new ElectionData(date.key.ElementAt(1));
                    singleYear.updateCity(date.key.ElementAt(0), date.values.ElementAt(0));
                    years.Add(singleYear);
                }
                else
                {
                    years.ElementAt(indexOfYears).updateCity(date.key.ElementAt(0), date.values.ElementAt(0));
                }
            }
            return years;
        }

        static int ContainsYear(string year, List<ElectionData> years)
        {
            for (int i = 0; i < years.Count; i++)
            {
                if (years.ElementAt(i).Year.Equals(year))
                {
                    return i;
                }
            }
            return -1;
        }

        static void showYears(List<ElectionData> years, JSON_DatabaseProporties proporties)
        {
            foreach(ElectionData data in years)
            {
                Console.WriteLine(data.getString(proporties));
            }
        }
    }
}
